import Xmobar

data Separator = Sep deriving (Read, Show)

instance Exec Separator where
    alias Sep = "s"
    run   Sep = return " <fc=#223333>|</fc> "

config :: Config
config = defaultConfig
       { font = "Lato 14px"
       , additionalFonts = ["linja pona, 22px"]
       , bgColor = "#111111"
       , fgColor = "#ccbbcc"
       , position = Static { xpos = 8, ypos = 8, width = 1920-8*2, height = 24 }
       , lowerOnStart = True
       , hideOnStart = False
       , persistent = True
       , iconRoot = ".config/xmobar/xpm/"
       , commands = [ Run $ Com "uname" ["-r"] "kern" 0
                    , Run $ Uptime
                        -- tenpo suno nanpa
                        ["-t"
                        , "<fn=1>\xe66b\xe664\xe63d</fn><days>"
                        ] 60
                    , Run $ Cpu
                        -- lawa pi ilo sona
                        ["-t"
                        , "<fn=1>\xe624\xe730\xe60e\xe63d</fn>: <total>%"
                        , "-H", "50", "--high", "#ffaaaa"
                        ] 20
                    , Run $ Memory
                        -- poki pali
                        ["-t"
                        , "<fn=1>\xe653\xf174</fn>: <used>M (<usedratio>%)"
                        ] 20
                    , Run $ DiskU
                        -- poki awen
                        [("/home"
                        , "<fn=1>\xe653\xf109</fn>: <free>")
                        ] [] 60
                    , Run $ Kbd
                        -- toki pi ilo sitelen
                        [ ("us", "<fn=1>\xe66c\xe730\xe619\xe660 [INLI]</fn>")
                        , ("ru", "<fn=1>\xe66c\xe730\xe619\xe660 [LOSI]</fn>")
                        ]
                    , Run $ Date
                        "<fn=1>\xe66b</fn> %d-%m-%Y %R"
                        -- tenpo
                        "date" 50
                    , Run $ Network "wg0"
                        ["-t"
                        -- akesi
                        , "<fn=1>\xe601</fn>"
                        ] 20
                    , Run Sep
                    , Run UnsafeXMonadLog
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       -- waso
       , template = " <action=`dmenu_run`><icon=la_ninpre.xpm/></action>\
                    \%UnsafeXMonadLog%}{\
                    \<fn=1>\xe674 [LINU]</fn> %kern%%s%\
                    \%uptime%%s%\
                    \<action=`alacritty -e htop`>%cpu%</action>%s%\
                    \<action=`alacritty -e htop`>%memory%</action>%s%\
                    \%disku%%s%\
                    \%kbd%%s%\
                    \<action=`alacritty -e calcurse`>%date%</action>%s%\
                    \%wg0%%s%"
       }

main :: IO ()
main = xmobar config
