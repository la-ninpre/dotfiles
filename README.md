# la-ninpre's dotfiles

i use gnu stow to deploy my dotfiles. installation is very straightforward:

``` shell
$ git clone https://git.aaoth.xyz/dotfiles.git
$ cd dotfiles
$ ./install
```

note that you will need to specify `--recurse-submodules` option if you also
want to have my xmonad config. for xmonad-specific installation instructions
refer to [it's repo][0].

## programs

a list of programs that i have configurations for.

* alacritty

* bash

* cwm

* doom-emacs

* kitty

* moc

* mpd

* neovim

* newsboat

* starship prompt

* xmobar (although it's only useful with xmonad. see above for details)

## scripts

a list of useful (for me) shell scripts also provided in current repo.

* `clean_local_ssh` -- purges localhost entries from `.ssh/known_hosts`.
  (yes it's very silly, but i need it because i ssh to my vms and ssh is
  always complaining about wrong host keys)

* `fossil-update` -- updates fossil to latest version

* `keylookup` -- receives a public key from keyserver (because gpg is not
  always able to do so for some reason)
  
* `nimi_sewi` -- a wrapper for [nimi sewi][1] that copies it's output to
  clipboard and gives a notification
  
* `paperbackup` -- makes a printable qr-code backup for gpg secret key

* `print_codes` -- makes a printable qr-code backup for otp recovery codes

* `rand` -- outputs random string

* `scrot_cmd` -- wrapper for scrot utility to simplify it's usage
  in keybindings

## license

all code in this repo is licensed under an isc license.

[0]:https://git.aaoth.xyz/xmonad-config.git
[1]:https://fsl.aaoth.xyz/nimisewi
