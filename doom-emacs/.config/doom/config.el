;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
;;
(setq user-full-name "la-ninpre")
(setq user-mail-address "leobrekalini@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:

(setq doom-theme 'la-ninpre)

(setq doom-font (font-spec :family "JetBrains Mono" :size 14 :weight 'medium :style 'medium)
      doom-big-font (font-spec :family "JetBrains Mono" :size 28 :weight 'medium)
      doom-unicode-font (font-spec :family "Noto")
      doom-serif-font (font-spec :family "Noto Serif")
      doom-variable-pitch-font (font-spec :family "Lato" :size 14))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; disable exit prompt
(setq confirm-kill-emacs nil)

;; fix path for pandoc
(add-to-list 'exec-path "~/.local/bin")

;; mu4e
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu4e")
(require 'mu4e)

(setq mu4e-change-filenames-when-moving t)
(setq message-send-mail-function 'smtpmail-send-it)
(setq mu4e-get-mail-command "mbsync -c ~/.config/mbsyncrc -a"
      mu4e-update-interval 300
      mu4e-sent-messages-behavior 'delete
      mu4e-attachment-dir "~/Downloads"
      message-send-mail-function 'smtpmail-send-it
      starttls-use-gnutls t
      mu4e-compose-format-flowed t)

(setq mu4e-context-policy 'pick-first)
(setq mu4e-compose-context-policy 'ask-if-none)

(add-to-list 'load-path "~/.config/doom")
(require 'la-ninpre-email)
;; actual `mu4e-contexts' is in =~/.config/doom/la-ninpre-email.el=.
;; i don't want to include it, but it looks very similar to this below.
;; also there's an example `mbsyncrc' in =~/.config/mbsyncrc-example=

;; (setq mu4e-contexts
;;   (list
;;    (make-mu4e-context
;;     :name "acct-1"
;;     :enter-func (lambda () (mu4e-message "Entering context acct-1"))
;;     :leave-func (lambda () (mu4e-message "Leaving context acct-1"))
;;     :match-func (lambda (msg)
;;                   (when msg
;;                     (mu4e-message-contact-field-matches
;;                      msg '(:from :to :cc :bcc) "acct-1@example.com")))
;;     :vars '((user-mail-address  . "acct-1@example.com")
;;             (user-full-name     . "acct-1")
;;             (mu4e-sent-folder   . "/acct-1/Sent")
;;             (mu4e-drafts-folder . "/acct-1/Drafts")
;;             (mu4e-trash-folder  . "/acct-1/Trash")
;;             (smtpmail-smtp-user . "acct-1@example.com")
;;             (smtpmail-starttls-credentials . (("smtp.example.com" 587 nil nil)))
;;             (smtpmail-auth-credentials
;;               . (expand-file-name "~/.password-store/acct-1.gpg"))
;;             (smtpmail-default-smtp-server . "smtp.example.com")
;;             (smtpmail-smtp-server         . "smtp.example.com")
;;             (smtpmail-smtp-service        . 587)
;;             (smtpmail-debug-info          . t)
;;             (smtpmail-debug-verbose       . t)
;;             (mu4e-maildir-shortcuts . (("/acct-1/INBOX"    . ?i)
;;                                        ("/acct-1/Sent"     . ?s)
;;                                        ("/acct-1/Trash"    . ?t)
;;                                        ("/acct-1/All Mail" . ?a)
;;                                        ("/acct-1/Starred"  . ?r)
;;                                        ("/acct-1/drafts"   . ?d)
;;                                        ))))
;;    (make-mu4e-context
;;     :name "acct-2"
;;     :enter-func (lambda () (mu4e-message "Entering context acct-2"))
;;     :leave-func (lambda () (mu4e-message "Leaving context acct-2"))
;;     :match-func (lambda (msg)
;;                   (when msg
;;                     (mu4e-message-contact-field-matches
;;                      msg '(:from :to :cc :bcc) "acct-2@example.com")))
;;     :vars '((user-mail-address  . "acct-2@example.com")
;;             (user-full-name     . "acct-2")
;;             (mu4e-sent-folder   . "/acct-2/[Gmail].Sent Mail")
;;             (mu4e-drafts-folder . "/acct-2/[Gmail].Drafts")
;;             (mu4e-trash-folder  . "/acct-2/[Gmail].Bin")
;;             (smtpmail-smtp-user . "acct-2@example.com")
;;             (smtpmail-starttls-credentials . (("smtp.gmail.com" 587 nil nil)))
;;             (smtpmail-auth-credentials
;;               . (expand-file-name "~/.password-store/self/acct-2.gpg"))
;;             (smtpmail-default-smtp-server . "smtp.gmail.com")
;;             (smtpmail-smtp-server         . "smtp.gmail.com")
;;             (smtpmail-smtp-service        . 587)
;;             (smtpmail-debug-info          . t)
;;             (smtpmail-debug-verbose       . t)
;;             (mu4e-maildir-shortcuts . (("/acct-2/INBOX"             . ?i)
;;                                        ("/acct-2/[Gmail].Sent Mail" . ?s)
;;                                        ("/acct-2/[Gmail].Bin"       . ?t)
;;                                        ("/acct-2/[Gmail].All Mail"  . ?a)
;;                                        ("/acct-2/[Gmail].Starred"   . ?r)
;;                                        ("/acct-2/[Gmail].drafts"    . ?d)
;;                                        ))))))
;; (provide 'la-ninpre-email)


(setq mml-secure-openpgp-signers '("DD1F79DC24054C1F42440F2E405C2C163EF6A2C9"))

;;(add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)

;; dired

(setq dired-listing-switches "-la --group-directories-first")

(defun xah-syntax-color-hex ()
  "syntax color text of the form `#ff1100' and #abc in current buffer.
url `http://ergoemacs.org/emacs/emacs_CSS_colors.html'
version 2017-03-12"
  (interactive)
  (font-lock-add-keywords
   nil
   '(("#[[:xdigit:]]\\{3\\}"
      (0 (put-text-property
          (match-beginning 0)
          (match-end 0)
          'face (list :background
                      (let* (
                             (ms (match-string-no-properties 0))
                             (r (substring ms 1 2))
                             (g (substring ms 2 3))
                             (b (substring ms 3 4)))
                        (concat "#" r r g g b b))))))
     ("#[[:xdigit:]]\\{6\\}"
      (0 (put-text-property
          (match-beginning 0)
          (match-end 0)
          'face (list :background (match-string-no-properties 0)))))))
  (font-lock-flush))

(add-hook 'emacs-lisp-mode 'xah-syntax-color-hex)
(add-hook 'css-mode-hook 'xah-syntax-color-hex)
(add-hook 'yaml-mode-hook 'xah-syntax-color-hex)
(add-hook 'html-mode-hook 'xah-syntax-color-hex)

(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)

(require 'rtags)
(cmake-ide-setup)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
