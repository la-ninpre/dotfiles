;;; la-ninpre-theme.el --- color theme by la-ninpre -*- lexical-binding: t; no-byte-compile: t; -*-

;; Author: la-ninpre
;; Maintainer: la-ninpre
;; Version: 0.1

;;; Commentary:

;; custom color theme by la-ninpre.
;; part of the code is taken from `doom-tomorrow-night' theme.
;; all colors are set by la-ninpre.

;;; Code:

(require 'doom-themes)

(defgroup la-ninpre-theme nil
  "Options for the `la-ninpre' theme."
  :group 'doom-themes)

(defcustom la-ninpre-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer
to deremine the exact padding"
  :group 'la-ninpre-theme
  :type '(choice integer boolean))

(def-doom-theme la-ninpre
  "Color theme by la-ninpre."

  ;; bg         gui             256             16
  ((bg          '("#19191a"     nil             nil))
   (bg-alt      '("#141414"     nil             nil))
   (base0       '("#232324"     "black"         "black"))
   (base1       '("#363638"     "#363638"       ))
   (base2       '("#3f3f42"     "#3f3f42"       ))
   (base3       '("#49494c"     "#49494c"       "brightblack"))
   (base4       '("#535356"     "#535356"       "brightblack"))
   (base5       '("#66666a"     "#66666a"       "brightblack"))
   (base6       '("#ceced2"     "#ceced2"       "brightblack"))
   (base7       '("#d3d3d7"     "#d3d3d7"       "brightblack"))
   (base8       '("#dddde1"     "#dddde1"       "white"))
   (fg          '("#cadcde"     "#cadcde"       "white"))
   (fg-alt      (doom-darken fg 0.4))

   (grey        '("#393838"     "#393838"       "brightblack"))
   (red         '("#ed7c68"     "#ed7c68"       "red"))
   (orange      '("#f9a776"     "#f9a776"       "brightred"))
   (yellow      '("#ffdb9d"     "#ffdb9d"       "yellow"))
   (green       '("#c7ea76"     "#c7ea76"       "green"))
   (teal        '("#7f9848"     "#7f9848"       "teal"))
   (blue        '("#a6d5fe"     "#a6d5fe"       "brightblue"))
   (dark-blue   '("#4e96d5"     "#4e96d5"       "blue"))
   (violet      '("#edbbe9"     "#edbbe9"       "brightmagenta"))
   (magenta     '("#bc5fa7"     "#edbbe9"       "magenta"))
   (dark-cyan   '("#7fe2c5"     "#7fe2c5"       "cyan"))
   (cyan        '("#55b795"     "#55b795"       "brightcyan"))

   (highlight           blue)
   (vertical-bar        base0)
   (selection           `(,(car (doom-lighten bg 0.1)) ,@(cdr base4)))
   (builtin             blue)
   (comments            grey)
   (doc-comments        (doom-lighten grey 0.14))
   (constants           orange)
   (functions           blue)
   (keywords            violet)
   (methods             blue)
   (operators           fg)
   (type                yellow)
   (strings             green)
   (variables           red)
   (numbers             orange)
   (region              selection)
   (error               red)
   (warning             yellow)
   (success             green)
   (vc-modified         fg-alt)
   (vc-added            green)
   (vc-deleted          red)

   ;; custom categories
   (modeline-bg     `(,(doom-darken (car bg-alt) 0.3) ,@(cdr base3)))
   (modeline-bg-alt `(,(car bg) ,@(cdr base1)))
   (modeline-fg     base8)
   (modeline-fg-alt comments)
   (-modeline-pad
    (when la-ninpre-padded-modeline
      (if (integerp la-ninpre-padded-modeline)
          la-ninpre-padded-modeline
        4))))

  ;; --- faces ------------------------------
  (((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground blue :bold bold)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-alt :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-alt)))

   ;;;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face :foreground violet)
   (rainbow-delimiters-depth-2-face :foreground blue)
   (rainbow-delimiters-depth-3-face :foreground orange)
   (rainbow-delimiters-depth-4-face :foreground green)
   (rainbow-delimiters-depth-5-face :foreground magenta)
   (rainbow-delimiters-depth-6-face :foreground yellow)
   (rainbow-delimiters-depth-7-face :foreground teal)
   ;;;; doom-modeline
   (doom-modeline-buffer-path       :foreground violet :bold bold)
   (doom-modeline-buffer-major-mode :inherit 'doom-modeline-buffer-path))

  ;; --- variables --------------------------
  ;; ()
  )

;;; la-ninpre-theme.el ends here
