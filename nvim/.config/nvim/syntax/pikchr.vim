" Vim syntax file
" Language: Pikchr
" Mantainer: la-ninpre
" Latest Revision: 14 Dec 2020

" check if syntax highlighting is not in conflict
if exists("b:current_syntax")
    finish
endif

""" syntax block

" keywords
syntax keyword PikchrStatement print assert
syntax keyword PikchrExprFunc abs cos dist int max min sin sqrt
syntax keyword PikchrObject arc arrow box circle cylinder dot ellipse file line
syntax keyword PikchrObject move oval spline text
syntax keyword PikchrDirection right down left up nextgroup=PikchrNumber
syntax keyword PikchrDirection n north ne e east se s south sw w west nw
syntax keyword PikchrPlace of in vertex
syntax keyword PikchrAttribute dashed dotted color fill behind cw ccw
syntax keyword PikchrAttribute thick thin solid chop fit
syntax keyword PikchrTextAttr above aligned below big small bold italic
syntax keyword PikchrTextAttr center ljust rjust
syntax keyword PikchrLocAttr at with
syntax keyword PikchrEdgename n north ne nw e east s south se sw w west
syntax keyword PikchrEdgename t top bot bottom c center start end left right 
syntax keyword PikchrEdgename nextgroup=PikchrPlace
syntax keyword PikchrNumPropty diameter ht height rad radius thickness width wid
syntax keyword PikchrNthObj last previous
syntax keyword PikchrOperator + - * /

" define stmt
syntax match PikchrDefineStmt "^define\s\i\+" contains=PikchrCustomObject
syntax keyword PikchrDefine define contained
syntax match PikchrCustomObject "\i\+" contained

" literals
syntax match PikchrComment "\v(#.*$)|(/\*.*\*/)"
syntax match PikchrString "\".*\""
syntax match PikchrVariable "\v\i+\s?(\*|\+|-|/)?\="
syntax match PikchrDollVar "\$\i\+"
syntax match PikchrLabel "\u\w*"
syntax match PikchrNumber "\v\.?\d+(\.\d+)?(\%|in|cm|px|pt|pc|mm)?"
syntax match PikchrNumber "\v0x\x+"
syntax match PikchrOrdinal "\v([^0]\d+(st|nd|rd|th))|first"

" matches
syntax match PikchrAttributeSup "\v<same(\sas)?>"
syntax match PikchrAttributeSup "\v<invis(ible)?>"
syntax match PikchrAttributeSup "\v\<-\>?|-\>"
syntax match PikchrPosition "\v(((of\sthe\s)?\sway\s)?between)|and"
syntax match PikchrDotEdgename "\v\.(n(e|w)?(orth)?|e(ast)?|s(e|w)?(outh)?|w(est)?)"
syntax match PikchrDotEdgename "\v\.(bot(tom)?|t(op)?|right|left|start|end|c(enter)?)"
syntax match PikchrDotPropty "\v\.(color|dashed|diameter|dotted|fill)"
syntax match PikchrDotPropty "\v\.(ht|height|rad(ius)?|thickness|wid(th)?)"
syntax match PikchrPathElem "\v<(from|then|go|to|(until\s)?even\swith|heading|close)>" nextgroup=PikchrDirection

""" highlight block

" keywords
hi def link PikchrStatement Statement
hi def link PikchrExprFunc Function
hi def link PikchrObject Type 
hi def link PikchrDirection Function
hi def link PikchrLocAttr Tag
hi def link PikchrPlace Function
hi def link PikchrTextAttr Identifier
hi def link PikchrNumPropty Identifier
hi def link PikchrEdgename Identifier
hi def link PikchrNthObj Constant
hi def link PikchrOperator Operator

" define stmt
hi def link PikchrDefine Define
hi def link PikchrCustomObject Type 

" literals
hi def link PikchrComment Comment
hi def link PikchrString String
hi def link PikchrVariable Identifier
hi def link PikchrDollVar Identifier
hi def link PikchrLabel Identifier
hi def link PikchrNumber Number
hi def link PikchrOrdinal Number

" matches
hi def link PikchrAttribute Tag
hi def link PikchrAttributeSup Tag
hi def link PikchrPosition Function
hi def link PikchrDotEdgename Identifier
hi def link PikchrDotPropty Identifier
hi def link PikchrPathElem Statement
