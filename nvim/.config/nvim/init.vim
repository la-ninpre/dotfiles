" la-ninpre init.vim "

" misc: {{{

if &shell =~# 'fish$'
    set shell=bash
endif

" use space as leader key
let mapleader=" "

" enter new era :D
set nocompatible
set encoding=utf-8

" enable hybrid numbers
set nu rnu

" mouse is bad, but sometimes is useful
set mouse=a

" this is for better file searching
set path=.,**
set wildmenu

" more intuitive splits
set splitbelow splitright

" search options
set nohlsearch incsearch ignorecase smartcase showmatch

" ruler
set rulerformat=%38(%{strftime('%F\ %H:%M')}%=%l/%L\ (%p%%)%=%c%=\ %)

" expand tabs to spaces
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab

" use autoindents
set smartindent

" replace default behaviour with undotree plugin
set noswapfile nobackup
set undodir=~/.local/share/nvim/undodir
set undofile

" actually, i don't remember what is this...
set hidden

" fix update time
set updatetime=50

" remind yourself about 80 column rule
set colorcolumn=81

" this could be used to show unprintable characters
set listchars=tab:>-,eol:$,space:•,trail:~

" }}}
" plugins: {{{

" pluggins are installed with vim plug
call plug#begin('~/.local/share/nvim/plugged')
"Plug 'ap/vim-css-color'
Plug 'cespare/vim-toml'
Plug 'dag/vim-fish'
Plug 'dense-analysis/ale'
Plug 'https://tildegit.org/sloum/gemini-vim-syntax'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'karolbelina/uxntal.vim'
Plug 'L3MON4D3/LuaSnip'
Plug 'mattn/emmet-vim'
Plug 'mbbill/undotree'
Plug 'neovim/nvim-lspconfig'
Plug 'preservim/nerdcommenter'
Plug 'saadparwaiz1/cmp_luasnip'
Plug 'tpope/vim-surround'
call plug#end()
filetype plugin indent on

" }}}
" colorscheme: {{{
" limelight
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240
hi ColorColumn ctermbg=grey

" }}}
" mappings: {{{

" easier navigation in splits
map <C-H> <C-W><C-H>
map <C-L> <C-W><C-L>
map <C-J> <C-W><C-J>
map <C-K> <C-W><C-K>

" replace all occurencies of the word under the cursor
nnoremap <leader>su :%s/\<<C-r><C-w>\>/

" show or hide undo tree
nnoremap <leader>u :UndotreeToggle<CR>

" open helptags
nnoremap <leader>h :Helptags<CR>

" open file with fzf
nnoremap <leader>o :Files<CR>

" easy source init.vim
nnoremap <leader><CR> :so ~/.config/nvim/init.vim<CR>
" easy open init.vim
nnoremap <leader>vc :e ~/.config/nvim/init.vim<CR>

" show or hide unprintable characters
nnoremap <leader>sl :set list<CR>
nnoremap <leader>sn :set nolist<CR>

" goyo
nnoremap <leader>go :Goyo<CR>

" actually magick!
" this moves higlighted block up or down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" alias for capturing group in command mode (for use with regexps)
cmap ;( \(\)<Left><Left>

" }}}
" autocommands: {{{

" disable undofiles for passwords (yikes!)
au BufWritePre /dev/shm/pass.* setlocal noundofile

" limelight on when goyo
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" fix wrong nvim size when starting in alacritty
autocmd VimEnter * :silent exec "!kill -s SIGWINCH $PPID"

" enable insert mode when entering git commit message
autocmd VimEnter */COMMIT_EDITMSG exec 'normal 1G' | startinsert

" highlight yanked text (needs nvim 0.5.x)
augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 40})
augroup END

" }}}
" lsp: {{{
lua <<EOF

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities()

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  --vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', vim.lsp.buf.formatting, bufopts)
end

lspconfig = require "lspconfig"
util = require "lspconfig/util"

lspconfig.clangd.setup {
  on_attach = on_attach,
  capabilities = capabilities
}

lspconfig.pyright.setup {
  on_attach = on_attach,
  capabilities = capabilities
}

lspconfig.gopls.setup {
  cmd = {"gopls", "serve"},
  filetypes = {"go", "gomod"},
  root_dir = util.root_pattern("go.work", "go.mod", ".git"),
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    gopls = {
      analyses = {
        unusedparams = true,
      },
      staticcheck = true,
    },
  },
}
lspconfig.hls.setup{
  on_attach = on_attach,
  root_dir = vim.loop.cwd,
  capabilities = capabilities,
  settings = {
    rootMarkers = {"./git/"}
  }
}

-- luasnip setup
local luasnip = require 'luasnip'

-- nvim-cmp setup
local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  },
}
EOF
" }}}
" vim: ft=vim fdm=marker:
