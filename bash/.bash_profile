#
# ~/.bash_profile
#

export PATH="${PATH}:$HOME/.gem/ruby/2.7.0/bin:$HOME/.local/bin"
export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="brave"
export TESSDATA_PREFIX=/usr/share/tessdata

[ -f "${HOME}/.bashrc" ] && . "${HOME}/.bashrc"
