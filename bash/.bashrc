#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# vi mode
set -o vi

# aliases
alias ls='ls --color=auto --group-directories-first'
alias ll='ls -lAh'
alias cat='bat -p'
alias mocp='mocp -M "~/.config/moc"'
alias ssh='TERM=rxvt ssh' # fix alacritty term issues
alias ..='cd ..'

alias config='git --work-tree=$HOME/Documents/dotfiles --git-dir=$HOME/Documents/dotfiles/.git'

alias brc='nvim ~/.bashrc && source ~/.bashrc'
alias vrc='nvim ~/.config/nvim/init.vim'

alias v='nvim'
alias fsl='fossil'

PS1='[\u@\h \W]\$ '

# use gpg authentication key for ssh auth
SSH_AUTH_SOCK=$( gpgconf --list-dirs agent-ssh-socket )
export SSH_AUTH_SOCK
gpgconf --launch gpg-agent

# record timestamps in history
HISTTIMEFORMAT="%F %T "
export HISTTIMEFORMAT

# fzf bindings and tweaks
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
# shellcheck disable=SC2016
export FZF_CTRL_T_COMMAND='$FZF_DEFAULT_COMMAND'

FOSSILS_HOME=$HOME/Documents/Fossils
export FOSSILS_HOME

# quickly cd into fossil repo
fslcd() {
    _fsl_checkout=$(fossil all ls -c | fzf +m)
    cd "$_fsl_checkout" || return
}

# starship prompt
eval "$(starship init bash)"

